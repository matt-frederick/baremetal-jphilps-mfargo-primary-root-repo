# Overview

This is the Primary Root Repository for Consumer Edge Platform (Anthos Retail Edge). This repository purpose
is to first create a homogenous cluster configuration, then set a desired state of configuration for many clusters.

This repository is intended to be used by a Platform Engineer or Operations Engineer. Other software desired are delegated out via (Cluster Trait Repositories)[https://gitlab.com/gcp-solutions-public/retail-edge/available-cluster-traits] or customer software via `RepoSync` or `RootSync` configurations set within this repository.

## To install

This repoistory is installed per each cluster during the provisioning cluster process. This is the only imperative command
during the provisioning phase post cluster creation.  Simply apply the following YAML and `kubectl apply -f <file>` to apply.

```yaml
# root-sync.yaml
apiVersion: configsync.gke.io/v1beta1
kind: RootSync
metadata:
  name: "primary-root-sync"
  namespace: config-management-system
spec:
  sourceFormat: "unstructured"
  git:
    repo: "https://gitlab.com/gcp-solutions-public/retail-edge/primary-root-repo-template.git"
    branch: "main"
    period: "60s"                                       # check for changes every minute
    dir: "/config/clusters/con-edge-cluster/meta/root"  # con-edge-cluster is the default Cluster name
    auth: "token"
    secretRef:
      name: "git-creds"
```